package net.skyestudios.dashnav;

import android.app.Activity;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;

/**
 * Created by arkeonet64 on 3/22/2018.
 */

public class Camera extends CameraDevice.StateCallback implements TextureView.SurfaceTextureListener {
    private String cameraId;
    private CameraManager cameraManager;
    private int cameraFacing;
    private Size size;
    private HandlerThread backgroundThread;
    private Handler backgroundHandler;
    CameraCaptureSession cameraCaptureSession;
    CameraDevice cameraDevice;
    private TextureView cameraView;
    private int rotation;
    private MediaRecorder mediaRecorder;
    private File media_root;
    private File media_file;
    private boolean isRecording;
    private Activity activity;
    private CaptureRequest.Builder captureRequestBuilder;


    public void setup(Activity activity, TextureView textureView) {
        cameraManager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        cameraFacing = CameraCharacteristics.LENS_FACING_BACK;
        this.activity = activity;

        rotation = activity.getWindowManager().getDefaultDisplay().getRotation();

        if (cameraManager != null) {
            try {
                for (String cameraId :
                        cameraManager.getCameraIdList()) {
                    CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);
                    if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == cameraFacing) {
                        StreamConfigurationMap streamConfigurationMap = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                        size = streamConfigurationMap.getOutputSizes(SurfaceTexture.class)[0];
                        this.cameraId = cameraId;
                    }
                }
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }
        cameraView = textureView;
        cameraView.setSurfaceTextureListener(this);
        isRecording = false;
    }

    private void configureMediaRecorder() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        media_root = activity.getExternalFilesDir(Environment.DIRECTORY_DCIM);
        if (media_root != null &&
                !media_root.exists()) {
            media_root.mkdirs();
        }
        media_file = new File(media_root, new SimpleDateFormat("yyyy.MM.dd @ HH:mm:ss z", Locale.US).format(new Date()) + ".mp4");
        mediaRecorder.setOutputFile(media_file.getPath());
        mediaRecorder.setVideoEncodingBitRate(10000000);
        mediaRecorder.setVideoFrameRate(60);
        mediaRecorder.setVideoSize(size.getWidth(), size.getHeight());
        mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        if (rotation == Surface.ROTATION_90 ||
                rotation == Surface.ROTATION_270) {
            mediaRecorder.setOrientationHint(rotation);
        }
        try {
            mediaRecorder.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void openBackgroundThread() {
        backgroundThread = new HandlerThread("dashnav_camera_thread");
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
    }

    private void closeBackgroundThread() {
        if (backgroundHandler != null) {
            backgroundThread.quitSafely();
            backgroundThread = null;
            backgroundHandler = null;
        }
    }

    private void startPreview() {
        try {
            closeCameraSession();


            SurfaceTexture surfaceTexture = cameraView.getSurfaceTexture();
            surfaceTexture.setDefaultBufferSize(size.getWidth(), size.getHeight());
            Surface previewSurface = new Surface(surfaceTexture);
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(previewSurface);


            Matrix matrix = new Matrix();
            RectF viewRect = new RectF(0, 0, cameraView.getWidth(), cameraView.getHeight());
            RectF bufferRect = new RectF(0, 0, size.getHeight(), size.getWidth());
            float centerX = viewRect.centerX();
            float centerY = viewRect.centerY();
            if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
                bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
                matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
                float scale = Math.max(
                        (float) cameraView.getHeight() / size.getHeight(),
                        (float) cameraView.getWidth() / size.getWidth());
                matrix.postScale(scale, scale, centerX, centerY);
                matrix.postRotate(90 * (rotation - 2), centerX, centerY);
            }
            cameraView.setTransform(matrix);

            cameraDevice.createCaptureSession(Collections.singletonList(previewSurface), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    if (cameraDevice == null)
                        return;

                    try {
                        Camera.this.cameraCaptureSession = cameraCaptureSession;

                        updatePreview();
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {

                }
            }, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void updatePreview() throws CameraAccessException {
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        backgroundThread = new HandlerThread("dashnav_camera_thread");
        backgroundThread.start();
        Camera.this.cameraCaptureSession.setRepeatingRequest(captureRequestBuilder.build(), null, backgroundHandler);
    }

    void startRecording() {
        try {
            configureMediaRecorder();
            closeCameraSession();

            SurfaceTexture surfaceTexture = cameraView.getSurfaceTexture();
            surfaceTexture.setDefaultBufferSize(size.getWidth(), size.getHeight());
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);

            List<Surface> surfaces = new ArrayList<>();

            Surface previewSurface = new Surface(surfaceTexture);

            surfaces.add(previewSurface);

            captureRequestBuilder.addTarget(previewSurface);


            Surface recorderSurface = mediaRecorder.getSurface();

            surfaces.add(recorderSurface);
            captureRequestBuilder.addTarget(recorderSurface);

            cameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Camera.this.cameraCaptureSession = cameraCaptureSession;

                    try {
                        updatePreview();
                        mediaRecorder.start();
                        Camera.this.isRecording = true;
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {

                }
            }, backgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void closeCameraSession() {
        if (cameraCaptureSession != null){
            cameraCaptureSession.close();
            cameraCaptureSession = null;
        }
    }

    void stopRecording() {
        isRecording = false;
        try {
            cameraCaptureSession.stopRepeating();
            cameraCaptureSession.abortCaptures();
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        mediaRecorder.stop();
        mediaRecorder.reset();
        mediaRecorder.release();
        mediaRecorder = null;
        startPreview();
    }

    void open() {
        try {
            cameraManager.openCamera(cameraId, this, backgroundHandler);
        } catch (CameraAccessException | SecurityException e) {
            e.printStackTrace();
        }
    }

    void close() {
        closeCameraSession();

        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
        startPreview();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

    }

    @Override
    public void onOpened(@NonNull CameraDevice cameraDevice) {
        this.cameraDevice = cameraDevice;
    }

    @Override
    public void onClosed(@NonNull CameraDevice camera) {
        super.onClosed(camera);
    }

    @Override
    public void onDisconnected(@NonNull CameraDevice cameraDevice) {
    }

    @Override
    public void onError(@NonNull CameraDevice cameraDevice, int i) {
        close();
        open();
        startPreview();
    }

    public boolean isRecording() {
        return isRecording;
    }
}
