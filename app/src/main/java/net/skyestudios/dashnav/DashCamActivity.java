package net.skyestudios.dashnav;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.TextureView;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;

import java.util.ArrayList;
import java.util.List;

public class DashCamActivity extends FragmentActivity implements OnMapReadyCallback, View.OnClickListener {

    private TextView speedTextView;
    private boolean isRecording;
    private TextureView cameraView;
    private Camera camera;
    private final static String recording_key = "RECORDING_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            isRecording = savedInstanceState.getBoolean(recording_key);
        } else {
            isRecording = false;
        }
    }

    private void init() {
        setContentView(R.layout.activity_dashcam);
        isRecording = false;
        speedTextView = findViewById(R.id.speed_TextView);
        cameraView = findViewById(R.id.cameraView);
        checkPermissions();


        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (locationManager != null) {
            try {
                Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

//                speedTextView.setText(String.valueOf(location.getSpeed() * 2.237f));

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0.5f, new LocationListener() {
                    @Override
                    public void onLocationChanged(Location location) {
                        speedTextView.setText(String.valueOf(location.getSpeed() * 2.237f));
                    }

                    @Override
                    public void onStatusChanged(String s, int i, Bundle bundle) {

                    }

                    @Override
                    public void onProviderEnabled(String s) {

                    }

                    @Override
                    public void onProviderDisabled(String s) {

                    }
                });
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }

    private void checkPermissions() {
        if (checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkCallingOrSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                checkCallingOrSelfPermission(Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED &&
                checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                checkCallingOrSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else { //Permissions already granted
            startCamera();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            isRecording = savedInstanceState.getBoolean(recording_key);
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
//        mMap = googleMap;
//
//        // Add a marker in Sydney and move the camera
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//
//        mMap.addMarker(new MarkerOptions().position(latLng).title(String.format("Marker @: %s", latLng.toString())));
//        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 21.0f));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.recordVideo_Button:
                if (!camera.isRecording()) {
                    view.setBackgroundResource(R.drawable.ic_stop);
                    camera.startRecording();
                    isRecording = true;
                } else {
                    view.setBackgroundResource(R.drawable.ic_record);
                    camera.stopRecording();
                    isRecording = false;
                }
                break;
            case R.id.map_Button:
                startActivity(new Intent(DashCamActivity.this, MapsActivity.class));
                break;
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        outState.putBoolean(recording_key, isRecording);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (camera == null) {
            init();
        } else if (camera.isRecording()) {
            findViewById(R.id.recordVideo_Button).setBackgroundResource(R.drawable.ic_stop);
        }
    }

    private void startCamera() {
        if (camera == null) {
            camera = new Camera();
            camera.setup(this, cameraView);
            camera.open();
        } else if (!camera.isRecording()) {

        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (camera != null &&
                !camera.isRecording()) {
            camera.close();
            camera = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 0:
                List<Integer> resultsList = new ArrayList<>();
                for (int num :
                        grantResults) {
                    resultsList.add(num);
                }
                if (!resultsList.contains(-1)) {
                    if (grantResults[0] == 0) {
//                        startCamera();
                    }
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {

    }
}

